#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#ifdef _WIN32
#include <io.h>
#elif defined(__unix__)
#include <unistd.h>
#endif

int main(int argc, char *argv[])
{
	if (argc > 1)
	{
		for (int i2 = 1; i2 < argc; i2++)
		{
			int fd;
			if (( fd = open(argv[i2], O_RDWR) ) < 0)
			{
				perror(argv[i2]);
			}
			else
			{
				printf("파일 %s 열기 성공, fd=%d\n", argv[i2], fd);
				close(fd);
			}
		}
	}
	else
	{
		printf("사용법: %s 파일명, ...\n", argv[0]);
	}

	exit(0);
}
