#pragma once

typedef struct
{
	int points[4][4];
	int curr_player;
	int lastX, lastY;
	int count;
} ticTacToeBoard;

typedef enum { false, true } bool;

void printBoard(const ticTacToeBoard* board);
void initBoard(ticTacToeBoard* board);
bool isEmpty(const ticTacToeBoard* board, int x, int y);
void mark(ticTacToeBoard* board, int x, int y);
bool boardFull(const ticTacToeBoard* board);
int winner(const ticTacToeBoard* board);