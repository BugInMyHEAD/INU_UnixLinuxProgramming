#include "ticTacToeBoard.h"
#include <stdio.h>
#include "../custumdef.h"

void printBoard(const ticTacToeBoard* board)
{
	for (int y = 0; y < 4; y++)
	{
		printf("|");
		for (int x = 0; x < 4; x++)
		{
			if (board->points[y][x] == 0)
				printf(" ");
			else if (board->points[y][x] == 1)
				printf("X");
			else if (board->points[y][x] == -1)
				printf("O");

			printf("|");
		}
		printf("\n");
	}
}

void initBoard(ticTacToeBoard* board)
{
	for (int y = 0; y < 4; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			board->points[y][x] = 0;
		}
	}
	board->curr_player = 1;
	board->count = 0;
}

bool isEmpty(const ticTacToeBoard* board, int x, int y)
{
	return board->points[y][x] == 0;
}

void mark(ticTacToeBoard* board, int x, int y)
{
	board->points[y][x] = board->curr_player;
	board->lastX = x;
	board->lastY = y;
	board->count++;
}

bool boardFull(const ticTacToeBoard* board)
{
	return board->count >= sizeof(board->points) / sizeof(**board->points);
}

int winner(const ticTacToeBoard* board)
{
	// assuming the board is a square
	for (size_t i2 = 0; i2 < ARRLEN(*board->points); i2++)
	{
		if (board->curr_player != board->points[board->lastY][i2])
		{
			for (i2 = 0; i2 < ARRLEN(board->points); i2++)
			{
				if (board->curr_player != board->points[i2][board->lastX])
				{
					if (board->lastX == board->lastY || board->lastX == ARRLEN(*board->points) - 1 - board->lastY)
					{
						for (i2 = 0; i2 < ARRLEN(*board->points); i2++)
						{
							if (board->curr_player != board->points[i2][i2])
							{
								for (i2 = 0; i2 < ARRLEN(*board->points); i2++)
								{
									if (board->curr_player != board->points[ARRLEN(*board->points) - 1 - i2][i2])
									{
										return 0;
									}
								}
							}
						}
					}
					else
					{
						return 0;
					}
				}
			}
		}
	}

	return board->curr_player;
}
