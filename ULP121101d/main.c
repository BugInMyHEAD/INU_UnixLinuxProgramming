#include <stdio.h>
#include "ticTacToeBoard.h"

int main()
{
	ticTacToeBoard b1;
	initBoard(&b1);
	printBoard(&b1);
	int x, y;

	/* while board not full */
	while (!boardFull(&b1))
	{
		/* Ask for current player to make move */
		if (b1.curr_player == 1)
			printf("Player X, ");
		else
			printf("Player O, ");
		printf("what is your move? ");

		/* loop to get valid move. */
		do
		{
			scanf("%d %d", &x, &y);
			/* if move out of bound, get input agin */
			if (!( ( 1 <= x ) && ( x <= 4 ) && ( 1 <= y ) && ( y <= 4 ) ))
			{
				printf("Out of bounds.\n");
				continue;
			}
			/* If empty make move */
			if (isEmpty(&b1, x - 1, y - 1))
			{
				mark(&b1, x - 1, y - 1);
				break; //done with this move
			}
			else
			{
				printf("You can't play there.\n");
			}
		} while (true);

		printBoard(&b1);
		if (winner(&b1) != 0) break;//if there is a winner, exit

		/* Swap player */
		b1.curr_player *= -1;
	}

	/* We're done either because there's a winner */
	/* or because the board is full. */
	if (winner(&b1) == 1)
		printf("X is the winner!\n");
	else if (winner(&b1) == -1)
		printf("O is the winner!\n");
	else
		printf("The game is a tie!\n");
}