#include <stdio.h>
#include <stdlib.h>

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )

typedef struct
{
	int points[4][4];
	int curr_player;
	int lastX, lastY;
	int count;
} ticTacToeBoard;

typedef enum { false, true } bool;

void printBoard(const ticTacToeBoard* board);
void initBoard(ticTacToeBoard* board);
bool isEmpty(const ticTacToeBoard* board, int x, int y);
void mark(ticTacToeBoard* board, int x, int y);
bool boardFull(const ticTacToeBoard* board);
int winner(const ticTacToeBoard* board);

void printBoard(const ticTacToeBoard* board)
{
	for (int y = 0; y < 4; y++)
	{
		printf("|");
		for (int x = 0; x < 4; x++)
		{
			if (board->points[y][x] == 0)
				printf(" ");
			else if (board->points[y][x] == 1)
				printf("X");
			else if (board->points[y][x] == -1)
				printf("O");

			printf("|");
		}
		printf("\n");
	}
}

void initBoard(ticTacToeBoard* board)
{
	for (int y = 0; y < 4; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			board->points[y][x] = 0;
		}
	}
	board->curr_player = 1;
	board->count = 0;
}

bool isEmpty(const ticTacToeBoard* board, int x, int y)
{
	return board->points[y][x] == 0;
}

void mark(ticTacToeBoard* board, int x, int y)
{
	board->points[y][x] = board->curr_player;
	board->lastX = x;
	board->lastY = y;
	board->count++;
}

bool boardFull(const ticTacToeBoard* board)
{
	return board->count >= sizeof(board->points) / sizeof(**board->points);
}

int winner(const ticTacToeBoard* board)
{
	// assuming the board is a square
	for (size_t i2 = 0; i2 < ARRLEN(*board->points); i2++)
	{
		if (board->curr_player != board->points[board->lastY][i2])
		{
			for (i2 = 0; i2 < ARRLEN(board->points); i2++)
			{
				if (board->curr_player != board->points[i2][board->lastX])
				{
					if (board->lastX == board->lastY || board->lastX == ARRLEN(*board->points) - 1 - board->lastY)
					{
						for (i2 = 0; i2 < ARRLEN(*board->points); i2++)
						{
							if (board->curr_player != board->points[i2][i2])
							{
								for (i2 = 0; i2 < ARRLEN(*board->points); i2++)
								{
									if (board->curr_player != board->points[ARRLEN(*board->points) - 1 - i2][i2])
									{
										return 0;
									}
								}
							}
						}
					}
					else
					{
						return 0;
					}
				}
			}
		}
	}

	return board->curr_player;
}

int main()
{
	ticTacToeBoard b1;
	initBoard(&b1);
	printBoard(&b1);
	int x, y;

	/* while board not full */
	while (!boardFull(&b1))
	{
		/* Ask for current player to make move */
		if (b1.curr_player == 1)
			printf("Player X, ");
		else
			printf("Player O, ");
		printf("what is your move? ");

		/* loop to get valid move. */
		do
		{
			scanf("%d %d", &x, &y);
			/* if move out of bound, get input agin */
			if (!( ( 1 <= x ) && ( x <= 4 ) && ( 1 <= y ) && ( y <= 4 ) ))
			{
				printf("Out of bounds.\n");
				continue;
			}
			/* If empty make move */
			if (isEmpty(&b1, x - 1, y - 1))
			{
				mark(&b1, x - 1, y - 1);
				break; //done with this move
			}
			else
			{
				printf("You can't play there.\n");
			}
		} while (true);

		printBoard(&b1);
		if (winner(&b1) != 0) break;//if there is a winner, exit

									/* Swap player */
		b1.curr_player *= -1;
	}

	/* We're done either because there's a winner */
	/* or because the board is full. */
	if (winner(&b1) == 1)
		printf("X is the winner!\n");
	else if (winner(&b1) == -1)
		printf("O is the winner!\n");
	else
		printf("The game is a tie!\n");
}